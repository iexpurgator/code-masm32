INCLUDE Irvine32.inc
INCLUDE macros.inc

.DATA
	hPathFile		BYTE "FileTracker32.dll", 1000 DUP(?)
	hFile HANDLE	?
	filename		BYTE 1000 DUP (?)
	filesize		DWORD ?
	buf				BYTE 1000 DUP(?)
	buf1			DWORD ?
	BYTERead		DWORD 0

	szSpace = 38
	spaceArr		BYTE 0Ah, 9h, szSpace DUP(20h)
	startNextHeader	DWORD 0
	currentAddress	DWORD 0

	countSection	DWORD 0
	asMax			DWORD 31 DUP(1)

	nmDosHeader		BYTE "e_magic",0,"e_cblp",0,"e_cp",0,"e_crlc",0,"e_cparhdr",
					0,"e_minalloc",0,"e_maxalloc",0,"e_ss",0,"e_sp",0,"e_csum",
					0,"e_ip",0,"e_cs",0,"e_lfarlc",0,"e_ovno",0,"e_res",
					0,"e_oemid",0,"e_oeminfo",0,"e_res2",0,"e_lfanew",0
	asDosHeader		DWORD 14 DUP(1), 4, 1, 1, 10, 1
	szDosHeader		DWORD 18 DUP(2), 4

	nmNtHeader		BYTE "Signature",0
	szNtHeader		DWORD 4

	nmFileHeader	BYTE "Machine",0,"NumberOfSections",
					0,"TimeDataStamp",0,"PointerToSymbolTable",0,"NumberOfSymbols",
					0,"SizeOfOptionalHeader",0,"Characteristics",0
	szFileHeader	DWORD 2, 2, 4, 4, 4, 2, 2

	nmOptionalHeader BYTE "Magic"
				   BYTE 0,"MajorLinkerVersion"
				   BYTE 0,"MinorLinkerVersion"
				   BYTE 0,"SizeOfCode"
				   BYTE 0,"SizeOfInitializedData"
				   BYTE 0,"SizeOfUninitializedData"
				   BYTE 0,"AddressOfEntryPoint"
				   BYTE 0,"BaseOfCode"
				   BYTE 0,"BaseOfData"
				   BYTE 0,"ImageBase"
				   BYTE 0,"SectionAlignment"
				   BYTE 0,"FileAlignment"
				   BYTE 0,"MajorOperatingSystemVersion"
				   BYTE 0,"MinorOperatingSystemVersion"
				   BYTE 0,"MajorImageVersion"
				   BYTE 0,"MinorImageVersion"
				   BYTE 0,"MajorSubsystemVersion"
				   BYTE 0,"MinorSubsystemVersion"
				   BYTE 0,"Win32VersionValue"
				   BYTE 0,"SizeOfImage"
				   BYTE 0,"SizeOfHeaders"
				   BYTE 0,"CheckSum"
				   BYTE 0,"Subsystem"
				   BYTE 0,"DllCharacteristics"
				   BYTE 0,"SizeOfStackReserve"
				   BYTE 0,"SizeOfStackCommit"
				   BYTE 0,"SizeOfHeapReserve"
				   BYTE 0,"SizeOfHeapCommit"
				   BYTE 0,"LoaderFlags"
				   BYTE 0,"NumberOfRvaAndSizes"
				   BYTE 0
	szOptionalHeader DWORD 2, 1, 1, 9 DUP (4), 6 DUP(2), 4 DUP (4), 2, 2, 6 DUP (4)

	nmDataDirectories BYTE "Export Directory RVA"
					BYTE 0,"Export Directory Size"
					BYTE 0,"Import Directory RVA"
					BYTE 0,"Import Directory Size"
					BYTE 0,"Resource Directory RVA"
					BYTE 0,"Resource Directory Size"
					BYTE 0,"Exception Directory RVA"
					BYTE 0,"Exception Directory Size"
					BYTE 0,"Security Directory Offset"
					BYTE 0,"Security Directory Size"
					BYTE 0,"Relocation Directory RVA"
					BYTE 0,"Relocation Directory Size"
					BYTE 0,"Debug Directory RVA"
					BYTE 0,"Debug Directory Size"
					BYTE 0,"Architecture Directory RVA"
					BYTE 0,"Architecture Directory Size"
					BYTE 0,"Reserved"
					BYTE 0,"Reserved"
					BYTE 0,"TLS Directory RVA"
					BYTE 0,"TLS Directory Size"
					BYTE 0,"Load Config Directory RVA"
					BYTE 0,"Load Config Directory Size"
					BYTE 0,"Bound Import Directory RVA"
					BYTE 0,"Bound Import Directory Size"
					BYTE 0,"Import Address Table Directory RVA"
					BYTE 0,"Import Address Table Directory Size"
					BYTE 0,"Delay Import Directory RVA"
					BYTE 0,"Delay Import Directory Size"
					BYTE 0,".NET Directory RVA"
					BYTE 0,".NET Directory Size"
					BYTE 0
	szDataDirectories DWORD	30 DUP (4)

	nmSectionHeader	BYTE "Virtual Size",0,"Virtual Address",
					0,"Raw Size",0,"Raw Address",0,"Relocations Address",
					0,"LineNumbers",0,"Relocations Number",
					0,"LineNumbers Number",0,"Characteristics",0
	szSectionHeader	DWORD 6 DUP(4), 2, 2, 4

	exportDirectoryRVA			DWORD 0
	sectionVirtualAddress_ExT	DWORD 0
	sectionRawAddress_ExT		DWORD 0
	nmExportTable	BYTE "Characteristics",0,"TimeDateStamp",0,"MajorVersion",
					0,"MinorVersion",0,"Name",0,"Base",
					0,"NumberOfFuntions",0,"NumberOfNames",
					0,"*AddressOfFunctions",0,"*AddressOfNames",
					0,"*AddressOfNameOrdinals",0
	szExportTable	DWORD 4, 4, 2, 2, 7 DUP(4)

	importDirectoryRVA			DWORD 0
	sectionVirtualAddress_IAT	DWORD 0
	sectionRawAddress_IAT		DWORD 0

.CODE
start:
	call crlf
	call main

main PROC
	mWrite "File name: "
	mov edx, offset hPathFile
	mov ecx, sizeof hPathFile
	call ReadString

	call crlf
	mov edx, offset hPathFile
	call OpenInputFile
	cmp eax, INVALID_HANDLE_VALUE
	jz failopen

	mov hFile, eax
	call getByte
	call sh_DosHeader
	call sh_PeHeader
	call sh_SectionHeader
	call importAddressTable
	call exportAddressTable
	jmp q000
failopen:
	mWrite < "Can't open file!", 0Ah, 0Dh >
q000:
	call WaitMsg
	push 0
	call ExitProcess
main ENDP

getByte PROC ithOff: DWORD, szRead: DWORD
	pushad
	pushad
	invoke SetFilePointer, hFile, ithOff, NULL, FILE_BEGIN
	invoke ReadFile, hFile, offset buf, szRead, offset BYTERead, NULL
	popad
	popad
	ret
getByte ENDP

writeRexHex PROC uses eax ebx ecx esi, szData: DWORD, addrBuf:DWORD
	;size <-- ebx | data <-- eax
	mov ecx, szData
	lea esi, addrBuf
	ret
writeRexHex ENDP

nextStr PROC ; strlen
    push ebp
    mov ebp, esp
    mov edx, [ebp + 8] ; get the start address of the string
    mov eax, 0         ; initialize length = 0

L1: mov bl, [edx]  ; move the i-th character to BL
    cmp bl, 0      ; if BL = 0, then the end of the string, break the loop
    je L1_out
    add edx, 1     ; otherwise move EDX to the address next character
    add eax, 1     ; increase the length
    jmp L1
L1_out:
	inc edx        ; over \0
    pop ebp
    ret
nextStr ENDP

writeSpace PROC USES ecx, numberDot: DWORD
	mov ecx, szSpace
	sub ecx, numberDot
	lp1: mWrite < "." >
	loop lp1
	ret
writeSpace ENDP

writeValue PROC uses ecx, curAddr:DWORD, loopcount:DWORD
	mov ecx, loopcount ; size a value
	mov ebx, curAddr ; address value
	push eax
	mWrite < "|" >
	mov eax, curAddr
	sub eax, ecx
	call WriteHex
	mWrite < "|  " >
	cmp ecx, 4
	jb cmp2
	mWrite < "DWORD  0x">
	jmp gone
	cmp2:
	cmp ecx, 2
	jb cmp1
	mWrite < "WORD   0x">
	jmp gone
	cmp1:
	mWrite < "BYTE   0x">
	gone:
	pop eax
	l3:
		push eax
		dec ebx ; --addr value
		push ebx
		;
		push eax
		push ebx
		call getByte
		;
		mov ebx, 1
		mov al, buf
		call WriteHexB
		mov startNextHeader, eax ; save Address
		;
		pop ebx
		pop eax
	loop l3
	ret
writeValue ENDP

sh_inf PROC uses ecx eax ebx edx, _Size_:DWORD, _arrCnt_:DWORD, _Name_:DWORD, _length_:DWORD, startAddr:DWORD
LOCAL storageAdd : DWORD
	mov eax, startAddr
	mov storageAdd, eax
	mov esi, _Size_
	mov edi, _arrCnt_ ; array? count value
	mov edx, _Name_
	mov ecx, _length_ ; total value
	mov ebx, 0
	l1:
		mWrite < 09h >
		call WriteString
		push edx
		call nextStr
		push ecx ; safe
		push ebx ; safe
		push eax ; write space
		call WriteSpace
		mov ecx, [edi] ; count value
		mov eax, [esi] ; size a value
		l2:
			add storageAdd, eax ; address value
			push eax
			push storageAdd
			call writeValue
			push edx
			cmp ecx, 1
			je gone
			mov edx, offset spaceArr
			call WriteString
			gone:
			pop edx
		loop l2
		mWrite < 0Ah >
		add esi, TYPE DWORD
		add edi, TYPE DWORD
		pop ebx
		pop ecx
	loop l1
	mov eax, storageAdd
	mov currentAddress, eax
	ret
sh_inf ENDP

sh_DosHeader PROC
	mWrite < "[+] DOS Header", 0Ah, 0Dh >
	push startNextHeader
	push lengthof szDosHeader 
	push offset nmDosHeader
	push offset asDosHeader
	push offset szDosHeader
	call sh_inf
	ret
sh_DosHeader ENDP

sh_PeHeader PROC uses eax
	call crlf
	mWrite < "[+] NT Header", 0Ah, 0Dh >
	push startNextHeader
	push lengthof szNtHeader 
	push offset nmNtHeader
	push offset asMax
	push offset szNtHeader
	call sh_inf
	
	; Get Number Of Sections
	mov eax, currentAddress
	add eax, 2
	push 2
	push eax
	call getByte
	mov ax, WORD PTR [buf]
	mov countSection, eax

	call crlf
	mWrite < "[+] File Header", 0Ah, 0Dh >
	push currentAddress
	push lengthof szFileHeader 
	push offset nmFileHeader
	push offset asMax
	push offset szFileHeader
	call sh_inf
	
	call crlf
	mWrite < "[+] Optional Header", 0Ah, 0Dh >
	push currentAddress
	push lengthof szOptionalHeader 
	push offset nmOptionalHeader
	push offset asMax
	push offset szOptionalHeader
	call sh_inf

	;get Exort Directory RVA
	push 4
	push currentAddress
	call getByte
	mov eax, DWORD PTR [buf]
	mov exportDirectoryRVA, eax
	;get Import Directory RVA
	mov eax, currentAddress
	add eax, 8
	push 4
	push eax
	call getByte
	mov eax, DWORD PTR [buf]
	mov importDirectoryRVA, eax

	call crlf
	mWrite < "[+] Data Directories", 0Ah, 0Dh >
	push currentAddress
	push lengthof szDataDirectories 
	push offset nmDataDirectories
	push offset asMax
	push offset szDataDirectories
	call sh_inf

	ret
sh_PeHeader ENDP

check_inrange_Import PROC uses eax ebx
LOCAL VisualSize : DWORD
	mov eax, currentAddress
	add eax, 8
	push 4
	push eax
	call getByte
	mov ebx, DWORD PTR [buf]
	mov VisualSize, ebx
	;
	add eax, 4
	push 4
	push eax
	call getByte
	mov ebx, DWORD PTR [buf]
	add eax, 8
	push 4
	push eax
	call getByte
	mov eax, DWORD PTR [buf]
	cmp importDirectoryRVA, ebx
	jl outt
	;
	push ebx
	push eax
	mov eax, VisualSize
	add ebx, eax
	cmp importDirectoryRVA, ebx
	pop eax
	jg out1
	;
	mov sectionRawAddress_IAT, eax
	pop ebx
	mov sectionVirtualAddress_IAT, ebx
	jmp outt
	out1:
	pop ebx
	outt:
	ret
check_inrange_Import ENDP

check_inrange_Export PROC uses eax ebx
LOCAL VisualSize : DWORD
	mov eax, currentAddress
	add eax, 8
	push 4
	push eax
	call getByte
	mov ebx, DWORD PTR [buf]
	mov VisualSize, ebx
	add eax, 4
	push 4
	push eax
	call getByte
	mov ebx, DWORD PTR [buf]
	add eax, 8
	push 4
	push eax
	call getByte
	mov eax, DWORD PTR [buf]
	cmp ExportDirectoryRVA, ebx
	jl outt
	;
	push ebx
	push eax
	mov eax, VisualSize
	add ebx, eax
	cmp ExportDirectoryRVA, ebx
	pop eax
	jg out1
	;
	mov sectionRawAddress_ExT, eax
	pop ebx
	mov sectionVirtualAddress_ExT, ebx
	jmp outt
	out1:
	pop ebx
	outt:
	ret
check_inrange_Export ENDP

sh_SectionHeader PROC uses eax ebx ecx
	call crlf
	mWrite < "[+] Section Header: " >
	mov eax, countSection
	call WriteDec
	mWrite < " sections", 0Ah, 0Dh >
	mov ebx, currentAddress
	add ebx, 8
	mov currentAddress, ebx
	mov ecx, eax
	l1:
		push ecx
		call crlf
		; NameSection BYTE [8]
		mWrite < " " >
		push 8
		push currentAddress
		call getByte
		mov edx, offset buf
		call WriteString
		;
		call check_inrange_Export
		call check_inrange_Import
		;
		mov ebx, currentAddress
		add ebx, 8
		mov currentAddress, ebx
		call crlf
		push currentAddress
		push lengthof szSectionHeader 
		push offset nmSectionHeader
		push offset asMax
		push offset szSectionHeader
		call sh_inf
		pop ecx
	loop l1
	ret
sh_SectionHeader ENDP

fomula_address_i PROC in_offset:DWORD
	mov eax, in_offset
	mov ebx, sectionVirtualAddress_IAT
	sub eax, ebx
	mov ebx, sectionRawAddress_IAT
	add ebx, eax
	push 4
	push ebx ;out_offset
	call getByte
	mov eax, DWORD PTR [buf] ;out_value
	ret
fomula_address_i ENDP

importAddressTable PROC uses eax ebx
LOCAL offset_thunk : DWORD
LOCAL offset_OFTs : DWORD
LOCAL value_OFTs : DWORD
	call crlf
	mWrite < "[+] Import Directory", 0Ah, 0Dh >
	; dllOFTs
	push importDirectoryRVA
	call fomula_address_i
	mov offset_thunk, ebx

bigloop:
	push eax ; value_thunk
	call fomula_address_i
	mov offset_OFTs, ebx
	mov value_OFTs, eax
;-----------------------------------
	mWrite < 0Ah," # " >
	mov eax, offset_thunk
	add eax, 0Ch
	;
	push 4
	push eax
	call getByte
	mov eax, DWORD PTR [buf]
	;
	push eax
	call fomula_address_i
	mov eax, ebx
	;
	push 100
	push eax
	call getByte
	mov edx, offset buf
	call WriteString
	mWrite < 0Ah, 09h, "Hint  Name", 0Ah >
	loopDLL:
		push value_OFTs
		call fomula_address_i
		push 65
		push ebx
		call getByte
		mov ax, WORD PTR [buf]
		mov ebx, 2
		mWrite < 09h >
		call WriteHexB
		mWrite < 020h, 020h >
		mov edx, offset [buf+2]
		call WriteString
		mWrite < 0Ah > 
		mov ebx, offset_OFTs
		add ebx, 4
		mov offset_OFTs, ebx
		;
		push 4
		push ebx
		call getByte
		;
		mov eax, DWORD PTR [buf]
		mov value_OFTs, eax
		cmp eax, 0
		je wegone
		jmp loopDLL
	wegone:
	;
	mov ebx, offset_thunk
	add ebx, 14h
	mov offset_thunk, ebx
	push 4
	push ebx
	call getByte
	mov eax, DWORD PTR [buf]
	cmp eax, 0
	je weout
	jmp bigloop
weout:
	ret
importAddressTable ENDP

fomula_address_e PROC uses ebx, in_offset:DWORD
	mov eax, in_offset
	mov ebx, sectionVirtualAddress_ExT
	sub eax, ebx
	mov ebx, sectionRawAddress_ExT
	add eax, ebx
	ret
fomula_address_e ENDP

mGetByte PROC
	push 4
	push ebx
	call getByte
	mov eax, DWORD PTR [buf]
	ret
mGetByte ENDP

exportAddressTable PROC uses eax ebx
LOCAL countNames : DWORD
LOCAL addrFunc : DWORD
LOCAL addrNames : DWORD
LOCAL addrNamesOrdinals : DWORD
	call crlf
	mWrite < "[+] Export Directory", 0Ah, 0Dh >
	cmp sectionVirtualAddress_ExT, 0
	je notFound
	cmp sectionRawAddress_Ext, 0
	jne canSee
notFound:
	call crlf
	mWrite < 09h, "Not Found Export Directory!", 0Ah, 0Ah, 0Dh >
	jmp q001
canSee:
	push exportDirectoryRVA
	call fomula_address_e
	push eax
	
	push eax
	push lengthof szExportTable
	push offset nmExportTable
	push offset asMax
	push offset szExportTable
	call sh_inf
	
	pop ebx
	add ebx, 18h
	call mGetByte
	mov countNames, eax
	add ebx, 4
	call mGetByte

	push eax
	call fomula_address_e
	mov addrFunc, eax

	add ebx, 4
	call mGetByte
	push eax
	call fomula_address_e
	mov addrNames, eax

	add ebx, 4
	call mGetByte
	push eax
	call fomula_address_e
	mov addrNamesOrdinals, eax
	
	call crlf
	mWrite < 09h, "Function RVA", 09h, "Name Ordinal", 09h, "Name", 0Ah >
	push addrNamesOrdinals
	push addrNames
	push addrFunc
	push countNames
	call detailExport
q001:
	ret
exportAddressTable ENDP

detailExport PROC uses eax ebx ecx,
			countNames : DWORD, addrFunc : DWORD,
			addrNames : DWORD, addrNamesOrdinals : DWORD
	mov ecx, countNames
loopFunc:
	mWrite < 09h >
	push 2
	push addrNamesOrdinals
	call getByte
	xor eax, eax
	mov ax, WORD PTR [buf]
	push eax

	mov ebx, 4
	mul ebx
	add eax, addrFunc
	push 4
	push eax
	call getByte
	mov eax, DWORD PTR [buf]
	call WriteHex
	mWrite < 09h >
	
	pop eax
	mov ebx, 2
	call WriteHexB
	add addrNamesOrdinals, 2

	mWrite < 09h, 09h >
	jmp WriteNames
	continueLoop:
	mWrite < 0Ah, 0Dh >
loop loopFunc
	jmp finishWrite

WriteNames:
	push 4
	push addrNames
	call getByte
	mov eax, DWORD PTR [buf]
	push eax
	call fomula_address_e
	;
	push 65
	push eax
	call getByte
	mov edx, offset buf
	call WriteString
	add addrNames, 4
	jmp continueLoop
finishWrite:
	ret
detailExport ENDP

END start